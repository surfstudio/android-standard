[TOC]
# Recycler-decorator Release Notes

## 0.5.0-alpha
##### Recycler-decorator
* ANDDEP-785 added Builder for RecyclerView.ItemDecoration and sample
* ANDDEP-785 added ktx for Decorator.Builder to use with EasyAdapter
* Add .gitignore
* fix gradle config for recycler-decorator-easyadapter
* Fix readme
