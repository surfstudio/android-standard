[TOC]
# Docs Release Notes
## 0.5.0-alpha
##### Docs
* Init tracking changes in RELEASE_NOTES
* Kotlin code style settings configuration.
* Init tracking changes in RELEASE_NOTES
* ANDDEP-785 added link to recycler-decorator lib
* Remove redundant links
* ANDDEP-928 Update logging docs